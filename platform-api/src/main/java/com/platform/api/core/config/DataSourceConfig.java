package com.platform.api.core.config;

import com.alibaba.druid.filter.config.ConfigTools;
import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import com.alibaba.druid.util.DruidPasswordCallback;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Druid数据源配置
 * @author
 *
 */
@Configuration
public class DataSourceConfig {

	@Value("${spring.datasource.url}")
	private String url;
	@Value("${spring.datasource.username}")
	private String userName;
	@Value("${spring.datasource.password}")
	private String pwd;
	@Value("${spring.datasource.driver-class-name}")
	private String driverClassName;
	@Value("${spring.datasource.public-key}")
	private String publicKey;

	@Value("${jdbc.druid.defaultAutoCommit}")
	private boolean defaultAutoCommit;

	@Value("${jdbc.druid.initialSize}")
	private int initialSize;

	@Value("${jdbc.druid.minIdle}")
	private int minIdle;

	@Value("${jdbc.druid.maxActive}")
	private int maxActive;

	@Value("${jdbc.druid.maxWait}")
	private int maxWait;

	@Value("${jdbc.druid.timeBetweenEvictionRunsMillis}")
	private int timeBetweenEvictionRunsMillis;

	@Value("${jdbc.druid.minEvictableIdleTimeMillis}")
	private int minEvictableIdleTimeMillis;

	@Value("${jdbc.druid.testWhileIdle}")
	private boolean testWhileIdle;

	@Value("${jdbc.druid.testOnBorrow}")
	private boolean testOnBorrow;

	@Value("${jdbc.druid.testOnReturn}")
	private boolean testOnReturn;

	@Value("${jdbc.druid.filters}")
	private String filters;

	@Value("${jdbc.druid.poolPreparedStatements}")
	private boolean poolPreparedStatements;

	@Value("${jdbc.druid.maxOpenPreparedStatements}")
	private int maxOpenPreparedStatements;

	@Value("${jdbc.druid.asyncInit}")
	private boolean asyncInit;

	//@Value("${jdbc.druid.connectionProperties}")
	//private String connectionProperties;

	@Value("${jdbc.druid.login-name}")
	private String loginName;

	@Value("${jdbc.druid.login-password}")
	private String loginPassWord;

	@Value("${jdbc.druid.whitelist}")
	private String whitelist;

	@Value("${jdbc.druid.resetEnable}")
	private String resetEnable;

	/**
	 * 数据源配置
	 *
	 * @return
	 */
	@SuppressWarnings("serial")
	@Bean
	public DataSource getDataSource() {
		DruidDataSource ds = new DruidDataSource();
		ds.setUrl(this.getUrl());
		ds.setUsername(this.getUserName());
		ds.setPassword(this.getPwd());
		ds.setDriverClassName(this.getDriverClassName());

		ds.setDefaultAutoCommit(this.isDefaultAutoCommit());
		ds.setMaxActive(this.getMaxActive());
		ds.setInitialSize(this.getInitialSize());
		ds.setMaxWait(this.getMaxWait());
		ds.setMinIdle(this.getMinIdle());

		ds.setTimeBetweenEvictionRunsMillis(this.getTimeBetweenEvictionRunsMillis());
		ds.setMinEvictableIdleTimeMillis(this.getMinEvictableIdleTimeMillis());

		ds.setTestWhileIdle(this.isTestWhileIdle());
		ds.setTestOnBorrow(this.isTestOnBorrow());
		ds.setTestOnReturn(this.isTestOnReturn());

		ds.setPoolPreparedStatements(this.isPoolPreparedStatements());
		ds.setMaxOpenPreparedStatements(this.getMaxOpenPreparedStatements());

		ds.setAsyncInit(this.isAsyncInit());
		ds.setPasswordCallback(new DruidPasswordCallback() {
			@Override
			public void setProperties(Properties properties) {
				super.setProperties(properties);
				try {
					setPassword(ConfigTools.decrypt(getPublicKey(), getPwd()).toCharArray());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		try {
			ds.setFilters(this.getFilters());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ds;
	}

	/**
	 * 事务配置
	 * @return
	 */
	@Bean
	public PlatformTransactionManager transactionManager() {
		DataSourceTransactionManager transactionManager = new DataSourceTransactionManager();
		transactionManager.setDataSource(getDataSource());
		return transactionManager;
	}

	/**
	 * 监控配置
	 *
	 * @return
	 */
	@Bean
	public ServletRegistrationBean<StatViewServlet> registrationBean() {
		ServletRegistrationBean<StatViewServlet> bean = new ServletRegistrationBean<StatViewServlet>(new StatViewServlet(), "/druid/*");
		/** 初始化参数配置，initParams **/
		bean.addInitParameter("allow", this.getWhitelist());// 多个ip逗号隔开
		bean.addInitParameter("loginUsername", this.getLoginName());
		bean.addInitParameter("loginPassword", this.getLoginPassWord());
		bean.addInitParameter("resetEnable", this.getResetEnable());
		return bean;
	}

	/**
	 *
	 * @return
	 */
	@Bean
	public FilterRegistrationBean<WebStatFilter> druidStatFilter() {
		FilterRegistrationBean<WebStatFilter> bean = new FilterRegistrationBean<WebStatFilter>(new WebStatFilter());
		/** 拦截所有Url **/
		bean.addUrlPatterns("/*");
		/** 过滤以下后缀的Url **/
		bean.addInitParameter("exclusions", "*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*");
		return bean;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getDriverClassName() {
		return driverClassName;
	}

	public void setDriverClassName(String driverClassName) {
		this.driverClassName = driverClassName;
	}

	public boolean isDefaultAutoCommit() {
		return defaultAutoCommit;
	}

	public void setDefaultAutoCommit(boolean defaultAutoCommit) {
		this.defaultAutoCommit = defaultAutoCommit;
	}

	public int getInitialSize() {
		return initialSize;
	}

	public void setInitialSize(int initialSize) {
		this.initialSize = initialSize;
	}

	public int getMinIdle() {
		return minIdle;
	}

	public void setMinIdle(int minIdle) {
		this.minIdle = minIdle;
	}

	public int getMaxActive() {
		return maxActive;
	}

	public void setMaxActive(int maxActive) {
		this.maxActive = maxActive;
	}

	public int getMaxWait() {
		return maxWait;
	}

	public void setMaxWait(int maxWait) {
		this.maxWait = maxWait;
	}

	public int getTimeBetweenEvictionRunsMillis() {
		return timeBetweenEvictionRunsMillis;
	}

	public void setTimeBetweenEvictionRunsMillis(int timeBetweenEvictionRunsMillis) {
		this.timeBetweenEvictionRunsMillis = timeBetweenEvictionRunsMillis;
	}

	public int getMinEvictableIdleTimeMillis() {
		return minEvictableIdleTimeMillis;
	}

	public void setMinEvictableIdleTimeMillis(int minEvictableIdleTimeMillis) {
		this.minEvictableIdleTimeMillis = minEvictableIdleTimeMillis;
	}

	public boolean isTestWhileIdle() {
		return testWhileIdle;
	}

	public void setTestWhileIdle(boolean testWhileIdle) {
		this.testWhileIdle = testWhileIdle;
	}

	public boolean isTestOnBorrow() {
		return testOnBorrow;
	}

	public void setTestOnBorrow(boolean testOnBorrow) {
		this.testOnBorrow = testOnBorrow;
	}

	public boolean isTestOnReturn() {
		return testOnReturn;
	}

	public void setTestOnReturn(boolean testOnReturn) {
		this.testOnReturn = testOnReturn;
	}

	public String getFilters() {
		return filters;
	}

	public void setFilters(String filters) {
		this.filters = filters;
	}

	public boolean isPoolPreparedStatements() {
		return poolPreparedStatements;
	}

	public void setPoolPreparedStatements(boolean poolPreparedStatements) {
		this.poolPreparedStatements = poolPreparedStatements;
	}

	public int getMaxOpenPreparedStatements() {
		return maxOpenPreparedStatements;
	}

	public void setMaxOpenPreparedStatements(int maxOpenPreparedStatements) {
		this.maxOpenPreparedStatements = maxOpenPreparedStatements;
	}

	public boolean isAsyncInit() {
		return asyncInit;
	}

	public void setAsyncInit(boolean asyncInit) {
		this.asyncInit = asyncInit;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getLoginPassWord() {
		return loginPassWord;
	}

	public void setLoginPassWord(String loginPassWord) {
		this.loginPassWord = loginPassWord;
	}

	public String getWhitelist() {
		return whitelist;
	}

	public void setWhitelist(String whitelist) {
		this.whitelist = whitelist;
	}

	public String getResetEnable() {
		return resetEnable;
	}

	public void setResetEnable(String resetEnable) {
		this.resetEnable = resetEnable;
	}

	public String getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}
}
