package com.platform.api.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "测试")
@RestController
@RequestMapping("/api/sysmenu")
public class SysMenuController {

    @Value("${spring.test}")
    private String test;

    @ApiOperation(value = "后台注册开户", notes = "后台注册开户")
    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public String adminRegister() {

        String size = "测试成功";
        String test = "注册成功，正在跳转登录"
        return test;
    }
}
