package com.platform.api;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @MapperScan 扫描mapper文件
 * @ComponentScan 外部jar包需要重新扫描
 * @EnableSwagger2 启用swagger2文档
 * @author
 *
 */
@MapperScan("com.platform.dao")
@ComponentScan(basePackages = {
		"com.platform.api","com.platform.service","com.platform.commons"})
@EnableSwagger2
//@EnableEurekaClient
@SpringBootApplication
public class Application {
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}

