package com.platform.commons.core.config;

import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonRedisSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.*;
import org.springframework.data.redis.serializer.RedisSerializer;

/**
 * redis配置
 * @author wangj
 *
 */
@Configuration
public class RedisConfig {

	/**
	 * 配置redisTemplate key/value 序列化方式
	 * @param jedisConnectionFactory
	 * @return
	 */
	@Bean(name = "redisTemplate")
	public RedisTemplate<String, Object> redisTemplate(JedisConnectionFactory jedisConnectionFactory, FastJsonConfig config) {
		RedisTemplate<String, Object> redisTemplate = new RedisTemplate<String, Object>();
		FastJsonRedisSerializer<Object> fastJsonRedisSerializer = new FastJsonRedisSerializer<>(Object.class);
		fastJsonRedisSerializer.setFastJsonConfig(config);
		redisTemplate.setKeySerializer(RedisSerializer.string()); // key采用字符串存储
		redisTemplate.setHashKeySerializer(RedisSerializer.string()); // hashKey采用字符串存储
		redisTemplate.setValueSerializer(fastJsonRedisSerializer);	// value采用fastjson自动序列化
		redisTemplate.setHashValueSerializer(fastJsonRedisSerializer);	// hash value采用fastjson自动序列化
		redisTemplate.setConnectionFactory(jedisConnectionFactory);	// 采用默认工厂配置
		redisTemplate.afterPropertiesSet();
		return redisTemplate;
	}
	
	@Bean(name = "hashOperation")
    public HashOperations<String, String, Object> hashOperations(RedisTemplate<String, Object> redisTemplate) {
        return redisTemplate.opsForHash();
    }
	
	@Bean(name = "valueOperation")
    public ValueOperations<String, Object> valueOperations(RedisTemplate<String, Object> redisTemplate) {
        return redisTemplate.opsForValue();
    }
	
	@Bean(name = "setOperation")
    public SetOperations<String, Object> setOperations(RedisTemplate<String, Object> redisTemplate) {
        return redisTemplate.opsForSet();
    }
	
	@Bean(name = "listOperation")
    public ListOperations<String, Object> listOperations(RedisTemplate<String, Object> redisTemplate) {
        return redisTemplate.opsForList();
    }
	
	@Bean(name = "zSetOperation")
    public ZSetOperations<String, Object> zSetOperations(RedisTemplate<String, Object> redisTemplate) {
        return redisTemplate.opsForZSet();
    }

}
